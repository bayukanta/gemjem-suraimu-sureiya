extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -500

const UP = Vector2(0,-1)

var velocity = Vector2()

onready var animator = self.get_node("Animator")
onready var sprite = self.get_node("Sprite")

func get_input():
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed('jump'):
		velocity.y = jump_speed
	if Input.is_action_pressed('right'):
		velocity.x += speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed
	if Input.is_action_pressed('attack'):
		animator.play("Attack1")

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	if global.lives == 1:
		get_input()
		velocity = move_and_slide(velocity, UP)
	else:
		set_process(false)
		animator.play("DIE")

func _process(delta):
	if velocity.y != 0 && animator.current_animation != "Attack1":
		animator.play("Jump")
	elif velocity.x != 0 && animator.current_animation != "Attack1":
		animator.play("Walk")
		if velocity.x > 0:
			sprite.scale.x = 2.282
		else:
			sprite.scale.x = -2.282
	elif velocity.x == 0 && animator.current_animation != "Attack1":
		animator.play("Idle")
	





func _on_PlayerHitbox_area_entered(area):
	if area.get_name() == "EnemyHitbox":
		global.lives -=1
	if (global.lives == 0):
		animator.play("DIE")
		
func game_over():
	get_tree().change_scene(str("res://Scenes/Game Over Screen.tscn"))


func _on_Finish_body_entered(body):
	get_tree().change_scene(str("res://Scenes/Game Win Screen.tscn"))
