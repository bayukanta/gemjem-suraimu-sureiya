extends LinkButton


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export(String) var scene_to_load

func _on_New_Game_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))



# Called when the node enters the scene tree for the first time.
	 # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _on_Retry_pressed():
	global.lives = 1
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))


func _on_LinkButton_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
