extends KinematicBody2D

export (int) var speed = 100
export (int) var GRAVITY = 1200
export (int) var jump_speed = -400

const UP = Vector2(0,-1)

var velocity = Vector2()

onready var animator = self.get_node("Animator")
onready var sprite = self.get_node("Sprite")
func _ready():
	set_physics_process(false)
	velocity.x = -speed
	
func _physics_process(delta):
	velocity.y += GRAVITY * delta
	if is_on_wall():
		velocity.x *=-1
	velocity.y = move_and_slide(velocity, Vector2.UP).y

func _process(delta):
	if velocity.x != 0:
		animator.play("walk")


func _on_Hitbox_area_entered(area):
	if area.get_name() == "SwordHit":
		get_node("EnemyHitbox/CollisionShape2D").set_deferred("disabled",true)
		velocity.x = 0
		animator.play("die")
	else:
		return

func die():
	queue_free()
